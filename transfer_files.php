<?php
/*
AUTHOR: Naelson G SARAIVA
DATA: 16/10/2019 FINISH 17:01
BEGIN 15/10/2019 18:00  STOPED 20:00

 */

//BEGIN CONFIG
$host = '127.0.0.1';
$db = 'hg2win06_workicecrm';
$user = 'hg2win06_2wins';
$pass = '2wins@1207';
$charset = 'utf8mb4';

$table = "fx_files"; //Table of files
$preAdd = "/" . "workice/storage/app/uploads/leads" . "/";
$addCustomDir = $preAdd . "*.pdf";
//END CONFIG

$root = dirname(__FILE__);
$countingBar = 0;

for ($i = 0; $i < strlen($root); $i++) {if ($root[$i] == "/") {$countingBar++;}}

$newDirRoot = '';
$num = 0;
for ($j = 0; $j < strlen($root); $j++) {

    if ($root[$j] == "/") {
        $num = $num + 1;
    }

    if ($num == 4) {
        break;
    }

    $newDirRoot .= $root[$j];
}

define('DB_HOST', $host);
define('DB_NAME', $db);
define('DB_USER', $user);
define('DB_PASS', $pass);
define('DB_CHARSET', $charset);

class Connect
{
    public $conn;
    private $attrIp;
    private $attrDb;
    private $user;
    private $password;
    private $charset;
    protected $data;
    public function __construct()
    {
        $this->getDataConn(DB_HOST, DB_NAME, DB_USER, DB_PASS, DB_CHARSET); //Load config
        $dsn = "mysql:host=$this->attrIp;dbname=$this->attrDb;charset=$this->charset"; // Define DNS
        $options = [PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION, PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC, PDO::ATTR_EMULATE_PREPARES => false];
        try { $this->conn = new PDO($dsn, $this->user, $this->password, $options); // Set DNS data USER and ATTR
        } catch (\PDOException $e) {throw new \PDOException($e->getMessage(), (int) $e->getCode());}
    }
    private function getDataConn($pIp, $pDb, $pUser, $pPw, $pCharSet)
    {$this->attrIp = $pIp;
        $this->attrDb = $pDb;
        $this->user = $pUser;
        $this->password = $pPw;
        $this->charset = $pCharSet;}
    public function checkStatus()
    {
        if ($this->conn) {echo "DB OK! <br>";} else {echo "DB FAILED <br>";}
    }
}

$arrayFilesPDF = glob($newDirRoot . $addCustomDir);

$db = new Connect();
$db->checkStatus();

if ($arrayFilesPDF == false) {
    echo "Directory incorrect, not find the files! <br>";
}

echo "Total files finded on directory: " . count($arrayFilesPDF);

$run = $db->conn->prepare("SELECT filename FROM $table ");
$run->execute();

$dbFileName = array();

$countForDB = 0;
foreach ($run->fetchAll() as $row) {

    $dbFileName[$countForDB] = $row['filename'];
    $countForDB = $countForDB + 1;

}

echo "<br>"; //Break line
echo "Total files Database: " . count($dbFileName);

/*DEBUG*/
// echo "<pre>";
// print_r($arrayFilesPDF);
// echo "</pre>";

$filesFull = array();
$n = 0;
foreach ($arrayFilesPDF as $row) {
    $filesFull[$n] = $row;
    $n = $n + 1;
}

$countB = array();
for ($i = 0; $i < count($filesFull); $i++) {

    $numBarFile = 0;
    for ($j = 0; $j < strlen($filesFull[$i]); $j++) {
        if ($filesFull[$i][$j] == "/") {
            $numBarFile = $numBarFile + 1;
            $countB[$i] = $numBarFile;
        }
    }
}

$filesNamesShortCurt = array();

for ($k = 0; $k < count($filesFull); $k++) {

    $bar = 0;
    for ($n = 0; $n < strlen($filesFull[$k]); $n++) {

        if ($filesFull[$k][$n] == "/") {
            $bar = $bar + 1;
        }

        if ($bar == $countB[$k] and $filesFull[$k][$n] != "/") {
            $filesNamesShortCurt[$k] .= $filesFull[$k][$n];
        }
    }
}

$filesForDelete = array();
$nIndex = 0;
for ($LI = 0; $LI < count($filesNamesShortCurt); $LI++) {

    $endLoop = 0;

    for ($LJ = 0; $LJ < count($dbFileName); $LJ++) {

        if ($filesNamesShortCurt[$LI] == $dbFileName[$LJ]) {
            break;
        }

        $endLoop = $endLoop + 1;

        if ($endLoop == count($dbFileName)) {
            $filesForDelete[$nIndex] = $filesNamesShortCurt[$LI];
            $nIndex = $nIndex + 1;
        }
    }

}

echo "<br>";
echo "QT files that don't exist detected in database: " . count($filesForDelete);
echo "<br>";

echo "Do you for debug and see all files names, so go to seach by this text on code, the code for uncomment is below this text!";
// echo "<pre>";
// print_r($filesForDelete);
// echo "</pre>";

echo "<br>";
echo "Directory defined to be deleted, first file like exemple <strong>" . $newDirRoot . $preAdd . $filesForDelete[0] . " </strong>";
echo "<br>";

if (isset($_POST['active'])) {

    $boolean = $_POST['active'];

    if ($boolean == 1) {

        for ($d = 0; $d < count($filesForDelete); $d++) {
            if (unlink($newDirRoot . $preAdd . $filesForDelete[$d]) === false) {
                echo "<br>";
                echo "Error on " . $filesForDelete[$d] . " File Check if was deleted!";
                echo "<br>";
            }
        }
        echo "<br>";
        echo "All Deleted! Go to filezila and refresh directory for if it was deleted!";
        echo "<br>";
    }
}

echo "<br>";
?>