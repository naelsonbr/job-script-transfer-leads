<style type="text/css">
    .style-info{
        text-align: center;
        color: green;
         background-color: black;
         height: 30px;
        line-height: 30px;
        width: 30%; margin: auto;
        border-top: solid 1px silver;
    }
</style>

<?php
/*
 *Author: DEVNA
 *begin: 2020/02/28
 *finished: 2020/03/02 15:00
 */
require 'dbworkice.php';
require 'dbws.php';

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

// echo "<pre>";
// print_r($all_lead_workice);
// echo "<pre>";

// echo "<pre>";
// print_r($read_lead_wp_deals->fetchAll());
// echo "<pre>";

$read_lead_wp_deals = $conn_wp_crm->prepare("SELECT * FROM crm_wp.bd_erp_crm_deals");
$lead_deals_success = $read_lead_wp_deals->execute();

$wp_deals = $read_lead_wp_deals->fetchAll();

//turn on = true
$turn = false;

//for ($i = 0; $i < 1; $i++) {
for ($i = 0; $i < count($wp_deals); $i++) {

    $id_main_erp_crm_deals = $wp_deals[$i]['contact_id'];
    $agent_main_erp_crm_deals = $wp_deals[$i]['owner_id'];
    $id_primarykey_erp_crm_deals = $wp_deals[$i]['id'];

    $read_lead_wp_peoplemeta = $conn_wp_crm->prepare("SELECT * FROM crm_wp.bd_erp_peoplemeta where meta_key = 'id_lead_crm' and erp_people_id = $id_main_erp_crm_deals ");
    $read_lead_wp_peoplemeta->execute();

    $all_peoplemeta = $read_lead_wp_peoplemeta->fetchAll();

    // echo "<pre>";
    // print_r($all_peoplemeta);
    // echo "<pre>";

    $id_old = $all_peoplemeta[0]['meta_value'];

    //echo $id_old . "<br>";

    $read_all_calls_workice = $conn_workice_crm->prepare("SELECT * FROM hg2win06_workicecrm.fx_calls where phoneable_id = $id_old ");
    $read_all_calls_workice->execute();
    $all_workice_calls = $read_all_calls_workice->fetchAll();

    // echo "<pre>";
    // print_r($all_workice_calls);
    // echo "</pre>";

    //echo $all_workice_calls[0]['result'];

    for ($j = 0; $j < count($all_workice_calls); $j++) {

        $array_wp_activities = array(
            'type' => 1,
            'title' => $all_workice_calls[$j]['subject'],
            'deal_id' => $id_primarykey_erp_crm_deals,
            'contact_id' => $id_main_erp_crm_deals,
            'company_id' => null,
            'created_by' => $all_workice_calls[$j]['duration'],
            'assigned_to_id' => $agent_main_erp_crm_deals,
            'start' => $all_workice_calls[$j]['created_at'],
            'end' => $all_workice_calls[$j]['duration'],
            'is_start_time_set' => 1,
            'note' => $all_workice_calls[$j]['description'] . "<br>" . $all_workice_calls[$j]['result'],
            'done_at' => null,
            'done_by' => null,
            'created_at' => $all_workice_calls[$j]['created_at'],
            'updated_at' => $all_workice_calls[$j]['updated_at'],
            'deleted_at' => $all_workice_calls[$j]['deleted_at'],

        );

        $insert_wp_activities = "INSERT INTO crm_wp.bd_erp_crm_deals_activities (
        type,
        title,
        deal_id,
        contact_id,
        company_id,
        created_by,
        assigned_to_id,
        start,
        end,
        is_start_time_set,
        note,
        done_at,
        done_by,
        created_at,
        updated_at,
        deleted_at)
        Value (
        :type,
        :title,
        :deal_id,
        :contact_id,
        :company_id,
        :created_by,
        :assigned_to_id,
        :start,
        :end,
        :is_start_time_set,
        :note,
        :done_at,
        :done_by,
        :created_at,
        :updated_at,
        :deleted_at)";

        if ($turn === true) {
            $run_insert_wp_activities = $conn_wp_crm->prepare($insert_wp_activities);
            $run_insert_wp_activities->execute($array_wp_activities);
        }

    }

}