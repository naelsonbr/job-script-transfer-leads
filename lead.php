<style type="text/css">
    .style-info{
        text-align: center;
        color: green;
         background-color: black;
         height: 30px;
        line-height: 30px;
        width: 30%; margin: auto;
        border-top: solid 1px silver;
    }
</style>

<?php
/*
 *Author: DEVNA
 *begin: 2020/02/28
 *finished: 2020/03/02 15:00
 */
require 'dbworkice.php';
require 'dbws.php';

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

$all_lead_workice = $read_all_lead_workice->fetchAll(); //fetchAll()[0]; it way but slow

//COMMENTED DEBUG
// echo "Amount full array is: " . count($all_lead_workice);

// echo "<pre>";
// print_r($all_lead_workice[0]);vcggcfukgfoi7tfyjkfuytfukyfguyfuyfufuf
// echo "<pre>";

//echo "Amount sub arrays is: ".count($all_lead_workice[0])/2;
//print_r($all_lead_workice[0]['name']);

//CONFIG of turn on execution for insert values in database worpress [FALSE=>OFF OR TRUE=>ON]
$turn = false;

$last_id_lead_people_wp = 0;
$lead_workice_id = 0;

if ($lead_success === true) {

    //for ($i = 0; $i < 1; $i++) {

        for ($i = 0; $i < count($all_lead_workice); $i++) {

        $lead_workice_id = $all_lead_workice[$i]['id'];
        $read_all_form_meta_workice = $conn_workice_crm->prepare("SELECT * FROM hg2win06_workicecrm.fx_formmeta where customizable_id = $lead_workice_id ");
        $read_all_form_meta_workice->execute();

        $all_form_meta_workice = $read_all_form_meta_workice->fetchAll();

        // debug lines commented
        // arrays form-meta
        // echo "<pre>";
        // print_r($all_form_meta_workice);
        // echo "<pre>";

        //echo count($all_form_meta_workice[0]);

        $dataInsert = array(
            'user_id' => 0,
            'first_name' => $all_lead_workice[$i]['name'],
            'email' => $all_lead_workice[$i]['email'],
            'phone' => '',
            'mobile' => $all_lead_workice[$i]['mobile'],
            'website' => $all_lead_workice[$i]['website'],
            'street_1' => $all_lead_workice[$i]['address1'],
            'street_2' => $all_lead_workice[$i]['address2'],
            'country' => 'BR',
            'currency' => 'BR',
            'life_stage' => 'lead',
            'hash' => sha1(microtime() . 'erp-subscription-form'),
            'created' => $all_lead_workice[$i]['created_at'],

        );

        switch ($all_lead_workice[$i]['sales_rep']) {

            case 1:
                $dataInsert['contact_owner'] = 1;
                $dataInsert['created_by'] = 1;
                break;

            case 2:
                $dataInsert['contact_owner'] = 1;
                $dataInsert['created_by'] = 1;
                break;

            case 3:
                $dataInsert['contact_owner'] = 11;
                $dataInsert['created_by'] = 11;
                break;

            case 4:
                $dataInsert['contact_owner'] = 10;
                $dataInsert['created_by'] = 10;
                break;

            case 5:
                $dataInsert['contact_owner'] = 9;
                $dataInsert['created_by'] = 9;
                break;

            case 6:
                $dataInsert['contact_owner'] = 8;
                $dataInsert['created_by'] = 5;
                break;

            case 22:
                $dataInsert['contact_owner'] = 12;
                $dataInsert['created_by'] = 12;
                break;

            default: //empty if don't exist
                $dataInsert['contact_owner'] = '';
                $dataInsert['created_by'] = '';
        }

        $insert_wp_bd_erp_peoples = "INSERT INTO crm_wp.bd_erp_peoples (
        user_id,
        first_name,
        email,
        phone,
        mobile,
        website,
        street_1,
        street_2,
        country,
        currency,
        life_stage,
        hash,
        contact_owner,
        created_by,
        created)
        Value (
        :user_id,
        :first_name,
        :email,
        :phone,
        :mobile,
        :website,
        :street_1,
        :street_2,
        :country,
        :currency,
        :life_stage,
        :hash,
        :contact_owner,
        :created_by,
        :created)";

        $inser_lead_success_ws = false;

        if ($turn === true) {

            $run_insert_lead_ws_people = $conn_wp_crm->prepare($insert_wp_bd_erp_peoples);

            $inser_lead_success_ws = $run_insert_lead_ws_people->execute($dataInsert);

            if ($inser_lead_success_ws === true) {
                $last_id_lead_people_wp = $conn_wp_crm->lastInsertId();
                echo '<div class="style-info">DBWP: LEAD HAVE BEEN INSERTED</div>';
            }

        }

        if ($inser_lead_success_ws === true) {

            echo '<div class="style-info">DBWP: SQL RETURNED LAST ID LEAD JUST NOW ' . $last_id_lead_people_wp . '</div>';

            $dataInsert_P_T_Relations = array(
                'people_id' => $last_id_lead_people_wp,
                'people_types_id' => 1,
            );

            $insert_wp_bd_erp_P_T_Relations = "INSERT INTO crm_wp.bd_erp_people_type_relations (people_id,people_types_id) Value (:people_id,:people_types_id)";

            if ($turn === true) {

                $run_insert_lead_ws_P_T_Relations = $conn_wp_crm->prepare($insert_wp_bd_erp_P_T_Relations);

                if ($run_insert_lead_ws_P_T_Relations->execute($dataInsert_P_T_Relations) === true) {

                    echo '<div class="style-info">DBWP: People Type Relations table inserted OK </div>';

                    // Parse if it's OK
                    switch ($all_lead_workice[$i]['stage_id']) {

                        case 63:
                            $run_insert_form_meta = $conn_wp_crm->prepare("INSERT INTO crm_wp.bd_erp_peoplemeta (erp_people_id,meta_key,meta_value) Value (:erp_people_id,:meta_key,:meta_value)");
                            $run_insert_form_meta->execute(array('erp_people_id' => $last_id_lead_people_wp, 'meta_key' => 'est_gio', 'meta_value' => 'Precatório 2021'));
                            break;

                        case 62:
                            $run_insert_form_meta = $conn_wp_crm->prepare("INSERT INTO crm_wp.bd_erp_peoplemeta (erp_people_id,meta_key,meta_value) Value (:erp_people_id,:meta_key,:meta_value)");
                            $run_insert_form_meta->execute(array('erp_people_id' => $last_id_lead_people_wp, 'meta_key' => 'est_gio', 'meta_value' => 'Interior do Estado'));
                            break;

                        case 61:
                            $run_insert_form_meta = $conn_wp_crm->prepare("INSERT INTO crm_wp.bd_erp_peoplemeta (erp_people_id,meta_key,meta_value) Value (:erp_people_id,:meta_key,:meta_value)");
                            $run_insert_form_meta->execute(array('erp_people_id' => $last_id_lead_people_wp, 'meta_key' => 'est_gio', 'meta_value' => 'Vendido'));
                            break;

                        case 60:
                            $run_insert_form_meta = $conn_wp_crm->prepare("INSERT INTO crm_wp.bd_erp_peoplemeta (erp_people_id,meta_key,meta_value) Value (:erp_people_id,:meta_key,:meta_value)");
                            $run_insert_form_meta->execute(array('erp_people_id' => $last_id_lead_people_wp, 'meta_key' => 'est_gio', 'meta_value' => 'Pré-Aprovado 2Wins'));
                            break;

                        case 59:
                            $run_insert_form_meta = $conn_wp_crm->prepare("INSERT INTO crm_wp.bd_erp_peoplemeta (erp_people_id,meta_key,meta_value) Value (:erp_people_id,:meta_key,:meta_value)");
                            $run_insert_form_meta->execute(array('erp_people_id' => $last_id_lead_people_wp, 'meta_key' => 'est_gio', 'meta_value' => 'Arquivado'));
                            break;

                        case 56:
                            $run_insert_form_meta = $conn_wp_crm->prepare("INSERT INTO crm_wp.bd_erp_peoplemeta (erp_people_id,meta_key,meta_value) Value (:erp_people_id,:meta_key,:meta_value)");
                            $run_insert_form_meta->execute(array('erp_people_id' => $last_id_lead_people_wp, 'meta_key' => 'est_gio', 'meta_value' => 'Aprovado'));
                            break;

                        case 55:
                            $run_insert_form_meta = $conn_wp_crm->prepare("INSERT INTO crm_wp.bd_erp_peoplemeta (erp_people_id,meta_key,meta_value) Value (:erp_people_id,:meta_key,:meta_value)");
                            $run_insert_form_meta->execute(array('erp_people_id' => $last_id_lead_people_wp, 'meta_key' => 'est_gio', 'meta_value' => 'Comprado pela original'));
                            break;

                        case 57:
                            $run_insert_form_meta = $conn_wp_crm->prepare("INSERT INTO crm_wp.bd_erp_peoplemeta (erp_people_id,meta_key,meta_value) Value (:erp_people_id,:meta_key,:meta_value)");
                            $run_insert_form_meta->execute(array('erp_people_id' => $last_id_lead_people_wp, 'meta_key' => 'est_gio', 'meta_value' => 'Negociação'));
                            break;

                    }

                    //pre defined
                    $run_insert_form_meta = $conn_wp_crm->prepare("INSERT INTO crm_wp.bd_erp_peoplemeta (erp_people_id,meta_key,meta_value) Value (:erp_people_id,:meta_key,:meta_value)");
                    $run_insert_form_meta->execute(array('erp_people_id' => $last_id_lead_people_wp, 'meta_key' => 'photo_id', 'meta_value' => 0));

                    $run_insert_form_meta = $conn_wp_crm->prepare("INSERT INTO crm_wp.bd_erp_peoplemeta (erp_people_id,meta_key,meta_value) Value (:erp_people_id,:meta_key,:meta_value)");
                    $run_insert_form_meta->execute(array('erp_people_id' => $last_id_lead_people_wp, 'meta_key' => 'id_lead_crm', 'meta_value' => $lead_workice_id));

                    //fix
                    $run_insert_form_meta = $conn_wp_crm->prepare("INSERT INTO crm_wp.bd_erp_peoplemeta (erp_people_id,meta_key,meta_value) Value (:erp_people_id,:meta_key,:meta_value)");
                    $run_insert_form_meta->execute(array('erp_people_id' => $last_id_lead_people_wp, 'meta_key' => 'fonte', 'meta_value' => 'Lista Depre')); // last step

                    //data-create
                    $run_insert_form_meta = $conn_wp_crm->prepare("INSERT INTO crm_wp.bd_erp_peoplemeta (erp_people_id,meta_key,meta_value) Value (:erp_people_id,:meta_key,:meta_value)");
                    $run_insert_form_meta->execute(array('erp_people_id' => $last_id_lead_people_wp, 'meta_key' => 'data_de_cadastro', 'meta_value' => $all_lead_workice[$i]['created_at']));

                    //value from lead
                    $run_insert_form_meta = $conn_wp_crm->prepare("INSERT INTO crm_wp.bd_erp_peoplemeta (erp_people_id,meta_key,meta_value) Value (:erp_people_id,:meta_key,:meta_value)");
                    $run_insert_form_meta->execute(array('erp_people_id' => $last_id_lead_people_wp, 'meta_key' => 'valor_da_lead', 'meta_value' => $all_lead_workice[$i]['computed_value']));

                    //parente 1
                    $run_insert_form_meta = $conn_wp_crm->prepare("INSERT INTO crm_wp.bd_erp_peoplemeta (erp_people_id,meta_key,meta_value) Value (:erp_people_id,:meta_key,:meta_value)");
                    $run_insert_form_meta->execute(array('erp_people_id' => $last_id_lead_people_wp, 'meta_key' => 'parente_1', 'meta_value' => $all_lead_workice[$i]['city']));

                    //parente 2
                    $run_insert_form_meta = $conn_wp_crm->prepare("INSERT INTO crm_wp.bd_erp_peoplemeta (erp_people_id,meta_key,meta_value) Value (:erp_people_id,:meta_key,:meta_value)");
                    $run_insert_form_meta->execute(array('erp_people_id' => $last_id_lead_people_wp, 'meta_key' => 'parente_2', 'meta_value' => $all_lead_workice[$i]['zip_code']));

                    //estado
                    $run_insert_form_meta = $conn_wp_crm->prepare("INSERT INTO crm_wp.bd_erp_peoplemeta (erp_people_id,meta_key,meta_value) Value (:erp_people_id,:meta_key,:meta_value)");
                    $run_insert_form_meta->execute(array('erp_people_id' => $last_id_lead_people_wp, 'meta_key' => 'ult__empresa_trabalhou', 'meta_value' => $all_lead_workice[$i]['state']));

                    //message
                    $run_insert_form_meta = $conn_wp_crm->prepare("INSERT INTO crm_wp.bd_erp_peoplemeta (erp_people_id,meta_key,meta_value) Value (:erp_people_id,:meta_key,:meta_value)");
                    $run_insert_form_meta->execute(array('erp_people_id' => $last_id_lead_people_wp, 'meta_key' => 'mensagem', 'meta_value' => $all_lead_workice[$i]['message']));

                    for ($j = 0; $j < count($all_form_meta_workice); $j++) {

                        //transfer workice to wp
                        switch ($all_form_meta_workice[$j]['meta_key']) {

                            case 'cabea':
                                $run_insert_form_meta = $conn_wp_crm->prepare("INSERT INTO crm_wp.bd_erp_peoplemeta (erp_people_id,meta_key,meta_value) Value (:erp_people_id,:meta_key,:meta_value)");
                                $run_insert_form_meta->execute(array('erp_people_id' => $last_id_lead_people_wp, 'meta_key' => 'cabe_a', 'meta_value' => str_replace("\\", "", $all_form_meta_workice[$j]['meta_value'])));
                                break;

                            case 'ordinria':
                                $run_insert_form_meta = $conn_wp_crm->prepare("INSERT INTO crm_wp.bd_erp_peoplemeta (erp_people_id,meta_key,meta_value) Value (:erp_people_id,:meta_key,:meta_value)");
                                $run_insert_form_meta->execute(array('erp_people_id' => $last_id_lead_people_wp, 'meta_key' => 'ordin_ria', 'meta_value' => str_replace("\\", "", $all_lead_workice[$i]['company'])));
                                break;

                            case 'execuo':
                                $run_insert_form_meta = $conn_wp_crm->prepare("INSERT INTO crm_wp.bd_erp_peoplemeta (erp_people_id,meta_key,meta_value) Value (:erp_people_id,:meta_key,:meta_value)");
                                $run_insert_form_meta->execute(array('erp_people_id' => $last_id_lead_people_wp, 'meta_key' => 'execu__o', 'meta_value' => str_replace("\\", "", $all_form_meta_workice[$j]['meta_value'])));
                                break;

                            case 'ep':
                                $run_insert_form_meta = $conn_wp_crm->prepare("INSERT INTO crm_wp.bd_erp_peoplemeta (erp_people_id,meta_key,meta_value) Value (:erp_people_id,:meta_key,:meta_value)");
                                $run_insert_form_meta->execute(array('erp_people_id' => $last_id_lead_people_wp, 'meta_key' => 'e_p_', 'meta_value' => str_replace("\\", "", $all_form_meta_workice[$j]['meta_value'])));
                                break;

                            case 'ordem':
                                $run_insert_form_meta = $conn_wp_crm->prepare("INSERT INTO crm_wp.bd_erp_peoplemeta (erp_people_id,meta_key,meta_value) Value (:erp_people_id,:meta_key,:meta_value)");
                                $run_insert_form_meta->execute(array('erp_people_id' => $last_id_lead_people_wp, 'meta_key' => 'ordem', 'meta_value' => str_replace("\\", "", $all_form_meta_workice[$j]['meta_value'])));
                                break;

                            case 'contra':
                                $run_insert_form_meta = $conn_wp_crm->prepare("INSERT INTO crm_wp.bd_erp_peoplemeta (erp_people_id,meta_key,meta_value) Value (:erp_people_id,:meta_key,:meta_value)");
                                $run_insert_form_meta->execute(array('erp_people_id' => $last_id_lead_people_wp, 'meta_key' => 'contra', 'meta_value' => str_replace("\\", "", $all_form_meta_workice[$j]['meta_value'])));
                                break;

                            case 'ndice-da-poca':
                                $run_insert_form_meta = $conn_wp_crm->prepare("INSERT INTO crm_wp.bd_erp_peoplemeta (erp_people_id,meta_key,meta_value) Value (:erp_people_id,:meta_key,:meta_value)");
                                $run_insert_form_meta->execute(array('erp_people_id' => $last_id_lead_people_wp, 'meta_key' => '_ndice_da__poca', 'meta_value' => str_replace("\\", "", $all_form_meta_workice[$j]['meta_value'])));
                                break;

                            case 'n-do-processo-depre':
                                $run_insert_form_meta = $conn_wp_crm->prepare("INSERT INTO crm_wp.bd_erp_peoplemeta (erp_people_id,meta_key,meta_value) Value (:erp_people_id,:meta_key,:meta_value)");
                                $run_insert_form_meta->execute(array('erp_people_id' => $last_id_lead_people_wp, 'meta_key' => 'n_mero_do_processo_depre', 'meta_value' => str_replace("\\", "", $all_form_meta_workice[$j]['meta_value'])));
                                break;

                            case 'data-base':
                                $run_insert_form_meta = $conn_wp_crm->prepare("INSERT INTO crm_wp.bd_erp_peoplemeta (erp_people_id,meta_key,meta_value) Value (:erp_people_id,:meta_key,:meta_value)");
                                $run_insert_form_meta->execute(array('erp_people_id' => $last_id_lead_people_wp, 'meta_key' => 'data_base', 'meta_value' => str_replace("\\", "", $all_form_meta_workice[$j]['meta_value'])));
                                break;

                            case 'advogado':
                                $run_insert_form_meta = $conn_wp_crm->prepare("INSERT INTO crm_wp.bd_erp_peoplemeta (erp_people_id,meta_key,meta_value) Value (:erp_people_id,:meta_key,:meta_value)");
                                $run_insert_form_meta->execute(array('erp_people_id' => $last_id_lead_people_wp, 'meta_key' => 'advogado', 'meta_value' => str_replace("\\", "", $all_form_meta_workice[$j]['meta_value'])));
                                break;

                            case 'link-tjsp':
                                $run_insert_form_meta = $conn_wp_crm->prepare("INSERT INTO crm_wp.bd_erp_peoplemeta (erp_people_id,meta_key,meta_value) Value (:erp_people_id,:meta_key,:meta_value)");
                                $run_insert_form_meta->execute(array('erp_people_id' => $last_id_lead_people_wp, 'meta_key' => 'link_tjsp', 'meta_value' => str_replace("\\", "", $all_form_meta_workice[$j]['meta_value'])));
                                break;

                            case 'telefone-comercial':
                                $run_insert_form_meta = $conn_wp_crm->prepare("INSERT INTO crm_wp.bd_erp_peoplemeta (erp_people_id,meta_key,meta_value) Value (:erp_people_id,:meta_key,:meta_value)");
                                $run_insert_form_meta->execute(array('erp_people_id' => $last_id_lead_people_wp, 'meta_key' => 'telefone_comercial', 'meta_value' => $all_form_meta_workice[$j]['meta_value']));
                                break;

                            case 'telefone-outros':
                                $run_insert_form_meta = $conn_wp_crm->prepare("INSERT INTO crm_wp.bd_erp_peoplemeta (erp_people_id,meta_key,meta_value) Value (:erp_people_id,:meta_key,:meta_value)");
                                $run_insert_form_meta->execute(array('erp_people_id' => $last_id_lead_people_wp, 'meta_key' => 'telefone_outros', 'meta_value' => $all_form_meta_workice[$j]['meta_value']));
                                break;

                            case 'telefone-serasa':
                                $run_insert_form_meta = $conn_wp_crm->prepare("INSERT INTO crm_wp.bd_erp_peoplemeta (erp_people_id,meta_key,meta_value) Value (:erp_people_id,:meta_key,:meta_value)");
                                $run_insert_form_meta->execute(array('erp_people_id' => $last_id_lead_people_wp, 'meta_key' => 'telefone_serasa', 'meta_value' => $all_form_meta_workice[$j]['meta_value']));
                                break;

                            case 'prioridade-de-pagamento':
                                $run_insert_form_meta = $conn_wp_crm->prepare("INSERT INTO crm_wp.bd_erp_peoplemeta (erp_people_id,meta_key,meta_value) Value (:erp_people_id,:meta_key,:meta_value)");
                                $run_insert_form_meta->execute(array('erp_people_id' => $last_id_lead_people_wp, 'meta_key' => 'prioridade_de_pagamento', 'meta_value' => str_replace("\\", "", $all_form_meta_workice[$j]['meta_value'])));
                                break;

                            case 'status-do-enriquecimento':
                                $run_insert_form_meta = $conn_wp_crm->prepare("INSERT INTO crm_wp.bd_erp_peoplemeta (erp_people_id,meta_key,meta_value) Value (:erp_people_id,:meta_key,:meta_value)");
                                $run_insert_form_meta->execute(array('erp_people_id' => $last_id_lead_people_wp, 'meta_key' => 'status_do_enriquecimento', 'meta_value' => str_replace("\\", "", $all_form_meta_workice[$j]['meta_value'])));
                                break;

                            case 'cpf':
                                $run_insert_form_meta = $conn_wp_crm->prepare("INSERT INTO crm_wp.bd_erp_peoplemeta (erp_people_id,meta_key,meta_value) Value (:erp_people_id,:meta_key,:meta_value)");
                                $run_insert_form_meta->execute(array('erp_people_id' => $last_id_lead_people_wp, 'meta_key' => 'cpf_cnpj', 'meta_value' => str_replace("\\", "", $all_form_meta_workice[$j]['meta_value'])));
                                break;

                            case 'valor-principal':
                                $run_insert_form_meta = $conn_wp_crm->prepare("INSERT INTO crm_wp.bd_erp_peoplemeta (erp_people_id,meta_key,meta_value) Value (:erp_people_id,:meta_key,:meta_value)");
                                $run_insert_form_meta->execute(array('erp_people_id' => $last_id_lead_people_wp, 'meta_key' => 'valor_principal', 'meta_value' => str_replace("\\", "", $all_form_meta_workice[$j]['meta_value'])));
                                break;

                            case 'juros':
                                $run_insert_form_meta = $conn_wp_crm->prepare("INSERT INTO crm_wp.bd_erp_peoplemeta (erp_people_id,meta_key,meta_value) Value (:erp_people_id,:meta_key,:meta_value)");
                                $run_insert_form_meta->execute(array('erp_people_id' => $last_id_lead_people_wp, 'meta_key' => 'juros', 'meta_value' => str_replace("\\", "", $all_form_meta_workice[$j]['meta_value'])));

                                break;

                            case 'telefone':
                                $telefone = $all_form_meta_workice[$j]['meta_value'];
                                $run_insert_form_meta = $conn_wp_crm->prepare("UPDATE crm_wp.bd_erp_peoples SET phone = :phone WHERE id = $last_id_lead_people_wp ");
                                $run_insert_form_meta->execute(array('phone' => $telefone));
                                break;

                            case 'controle':
                                $run_insert_form_meta = $conn_wp_crm->prepare("INSERT INTO crm_wp.bd_erp_peoplemeta (erp_people_id,meta_key,meta_value) Value (:erp_people_id,:meta_key,:meta_value)");
                                $run_insert_form_meta->execute(array('erp_people_id' => $last_id_lead_people_wp, 'meta_key' => 'controle', 'meta_value' => str_replace("\\", "", $all_form_meta_workice[$j]['meta_value'])));
                                break;

                        }

                    }

                } else {

                    echo "error insert people type relatios table";

                }
            }

        } else {

            echo "error insert lead in wordpress crm!";

        }

    }
} else {

    echo "Error read sql lead";

}
