<style type="text/css">
    .style-info{
        text-align: center;
        color: green;
         background-color: black;
         height: 30px;
        line-height: 30px;
        width: 30%; margin: auto;
        border-top: solid 1px silver;
    }
</style>

<?php
require 'dbworkice.php';
require 'dbws.php';

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

// echo "<pre>";
// print_r($all_lead_workice);
// echo "<pre>";

// echo "<pre>";
// print_r($read_lead_wp_deals->fetchAll());
// echo "<pre>";

$read_lead_wp_deals = $conn_wp_crm->prepare("SELECT * FROM crm_wp.bd_erp_crm_deals");
$lead_deals_success = $read_lead_wp_deals->execute();

$wp_deals = $read_lead_wp_deals->fetchAll();

//turn on = true
$turn = true;

//for ($i = 0; $i < 1; $i++) {
for ($i = 0; $i < count($wp_deals); $i++) {

    $id_main_erp_crm_deals = $wp_deals[$i]['contact_id'];
    $id_primarykey_erp_crm_deals = $wp_deals[$i]['id'];

    $read_lead_wp_peoplemeta = $conn_wp_crm->prepare("SELECT * FROM crm_wp.bd_erp_peoplemeta where meta_key = 'id_lead_crm' and erp_people_id = $id_main_erp_crm_deals ");
    $read_lead_wp_peoplemeta->execute();

    $all_peoplemeta = $read_lead_wp_peoplemeta->fetchAll();

    // echo "<pre>";
    // print_r($all_peoplemeta);
    // echo "<pre>";

    $id_old = $all_peoplemeta[0]['meta_value'];

    $read_all_files_workice = $conn_workice_crm->prepare("SELECT * FROM hg2win06_workicecrm.fx_files where uploadable_id = $id_old ");
    $read_all_files_workice->execute();
    $all_workice_files = $read_all_files_workice->fetchAll();

    // echo "<pre>";
    // print_r($all_workice_files);
    // echo "<pre>";

    $post_id = 0;
    $value_metapost = '';
    $value_attachment_datacreate = '';

   // for ($jk = 0; $jk < 1; $jk++) {
 for ($jk = 0; $jk < count($all_workice_files); $jk++) {

        //echo $all_workice_files[$jk]['ext'];


        if (!empty($all_workice_files[$jk]['created_at'])) {

            $array_wp_post = array(
                'post_author' => 1,
                'post_date' => $all_workice_files[$jk]['created_at'],
                'post_date_gmt' => $all_workice_files[$jk]['created_at'],
                'post_content' => $all_workice_files[$jk]['adapter'],
                'post_title' => $all_workice_files[$jk]['title'],
                'post_status' => 'inherit',
                'comment_status' => 'open',
                'ping_status' => 'closed',
                'guid' => 'http://crmwp.2wins.com.br/wp-content/uploads/erp-deals-uploads/2020/03/' . $all_workice_files[$jk]['filename'],
                'post_type' => 'attachment',
                'post_mime_type' => $all_workice_files[$jk]['ext'],
            );

            $value_metapost = $all_workice_files[$jk]['filename']; // add global var
            $value_attachment_datacreate = $all_workice_files[$jk]['created_at']; // add global var
            $insert_wp_post = "INSERT INTO crm_wp.bd_posts (post_author,post_date,post_date_gmt,post_content,post_title,post_status,comment_status,ping_status,guid,post_type,post_mime_type)Value (:post_author,:post_date,:post_date_gmt,:post_content,:post_title,:post_status,:comment_status,:ping_status,:guid,:post_type,:post_mime_type)";

            if ($turn === true) {

                $run_insert_wp_post = $conn_wp_crm->prepare($insert_wp_post);
                $run_insert_wp_post->execute($array_wp_post);
                $post_id = $conn_wp_crm->lastInsertId();

            }

            $array_wp_post = array(
                'post_id' => $post_id,
                'meta_key' => '_wp_attached_file',
                'meta_value' => $value_metapost,
            );

            $insert_wp_post = "INSERT INTO crm_wp.bd_postmeta (post_id,meta_key,meta_value)Value (:post_id,:meta_key,:meta_value)";

            if ($turn === true) {
                $run_insert_wp_post = $conn_wp_crm->prepare($insert_wp_post);
                $run_insert_wp_post->execute($array_wp_post);
            }

            $array_wp_attchment = array(
                'deal_id' => $id_primarykey_erp_crm_deals,
                'attachment_id' => $post_id,
                'added_by' => 1,
                'created_at' => $value_attachment_datacreate,
            );

            $insert_wp_attachments = "INSERT INTO crm_wp.bd_erp_crm_deals_attachments (deal_id,attachment_id,added_by,created_at)Value (:deal_id,:attachment_id,:added_by,:created_at)";

            if ($turn === true) {
                $run_insert_wp_attachments = $conn_wp_crm->prepare($insert_wp_attachments);
                $run_insert_wp_attachments->execute($array_wp_attchment);
            }
        }
    }
}